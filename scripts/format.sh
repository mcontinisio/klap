#!/bin/bash

clang-format --version

git ls-files -- '*.cpp' '*.hpp' | xargs clang-format -sort-includes -i -style=file

git diff --exit-code --color
