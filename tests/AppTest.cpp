#include "klap/App.hpp"

#include <gtest/gtest.h>

class AppTest : public testing::Test {};

TEST(AppTest, add_subcommand) {
    klap::App parent{"parent"};
    auto child = parent.add_subcommand("child");
    ASSERT_EQ(child->get_name(), parent.get_subcommand("child")->get_name());
    ASSERT_EQ(parent.get_name(), child->get_parent()->get_name());
}
