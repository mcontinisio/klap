#include "klap/Klap.hpp"

int main(int argc, char* argv[]) {
    klap::set_log_callback([](klap::LogLevel level, const std::string& message) {
        std::cout << "[" << level << "] " << message << std::endl;
    });

    klap::Shell shell{"prompt", "prompt demo"};
    shell.show_hints(false);

    shell.add_subcommand("login")->callback([]() {
        auto username = klap::Prompt("Username: ").read<std::string>();
        auto password = klap::Prompt("Password: ").read_password();
        std::cout << username << ", " << password << std::endl;
    });

    shell.add_subcommand("animals")->callback([]() {
        auto animal = klap::Prompt("Favorite animal?").choices({"dog", "pig", "cow"})->choose();
        std::cout << "favorite animal: " << animal << std::endl;
    });

    shell.run(argc, argv);

    return 0;
}
