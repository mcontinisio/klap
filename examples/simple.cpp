#include "klap/KlapHeaderOnly.hpp"

int main(int argc, char* argv[]) {
    klap::Shell shell{"simple", "a simple shell"};

    {
        shell.add_subcommand("hi")->callback([]() { std::cout << "hi" << std::endl; });
    }

    {
        std::string s;
        auto echoCommand = shell.add_subcommand("echo");
        echoCommand->autocomplete({"dog", "cow", "pig"});
        echoCommand->add_option("string", s, "string to echo")->required();
        echoCommand->callback([&s]() {
            std::cout << s << std::endl;
            s = {};
        });
    }

    shell.run(argc, argv);

    return 0;
}
