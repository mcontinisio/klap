#pragma once

#include <functional>
#include <iostream>

namespace klap {

enum class LogLevel { Trace, Debug, Info, Warning, Error, Critical };

inline std::ostream& operator<<(std::ostream& os, const LogLevel& logLevel) {
    switch (logLevel) {
    case LogLevel::Trace:
        os << "TRACE";
        break;
    case LogLevel::Debug:
        os << "DEBUG";
        break;
    case LogLevel::Info:
        os << "INFO";
        break;
    case LogLevel::Warning:
        os << "WARNING";
        break;
    case LogLevel::Error:
        os << "ERROR";
        break;
    case LogLevel::Critical:
        os << "CRITICAL";
        break;
    default:
        os << "UNNKNOWN";
        break;
    }
    return os;
}

namespace detail {

std::function<void(LogLevel level, const std::string& message)> log_callback;

void log(LogLevel level, const std::string& message) {
    if (log_callback) {
        log_callback(level, message);
    }
}

#define KLAP_LOG(level, message)                                                                                       \
    {                                                                                                                  \
        std::stringstream stream;                                                                                      \
        stream << message;                                                                                             \
        klap::detail::log(level, stream.str());                                                                        \
    }

#define KLAP_TRACE(message)                                                                                            \
    {                                                                                                                  \
        std::stringstream stream;                                                                                      \
        stream << message;                                                                                             \
        klap::detail::log(LogLevel::Trace, stream.str());                                                              \
    }

#define KLAP_DEBUG(message)                                                                                            \
    {                                                                                                                  \
        std::stringstream stream;                                                                                      \
        stream << message;                                                                                             \
        klap::detail::log(LogLevel::Debug, stream.str());                                                              \
    }

#define KLAP_INFO(message)                                                                                             \
    {                                                                                                                  \
        std::stringstream stream;                                                                                      \
        stream << message;                                                                                             \
        klap::detail::log(LogLevel::Info, stream.str());                                                               \
    }

#define KLAP_WARNING(message)                                                                                          \
    {                                                                                                                  \
        std::stringstream stream;                                                                                      \
        stream << message;                                                                                             \
        klap::detail::log(LogLevel::Warning, stream.str());                                                            \
    }

#define KLAP_ERROR(message)                                                                                            \
    {                                                                                                                  \
        std::stringstream stream;                                                                                      \
        stream << message;                                                                                             \
        klap::detail::log(LogLevel::Error, stream.str());                                                              \
    }

#define KLAP_CRITICAL(message)                                                                                         \
    {                                                                                                                  \
        std::stringstream stream;                                                                                      \
        stream << message;                                                                                             \
        klap::detail::log(LogLevel::Critical, stream.str());                                                           \
    }

} // namespace detail

/// Set the logging callback
void set_log_callback(std::function<void(LogLevel level, const std::string& message)> callback) {
    detail::log_callback = std::move(callback);
}

} // namespace klap
