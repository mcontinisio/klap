#pragma once

#include "klap/Log.hpp"
#include "klap/Option.hpp"

#include "CLI/CLI.hpp"

#include <string>
#include <vector>

namespace klap {

using CLI::Formatter;

class App : public CLI::App {
  public:
    App(std::string name, std::string description = "", App* parent = nullptr);

    /// Add child command
    App* add_subcommand(std::string name, std::string description = "");

    /// Add options
    Option* add_option(
        std::string name,
        CLI::callback_t callback,
        std::string description = "",
        bool defaulted = false);
    using CLI::App::add_option;

    /// Custom autocomplete
    App* autocomplete(std::vector<std::string> strings);

    /// Get child command
    App* get_subcommand(const std::string& name) const;

    /// Get the parent of this subcommand (or nullptr if master app)
    App* get_parent();

    /// Get the parent of this subcommand (or nullptr if master app) (const version)
    const App* get_parent() const;

    /// Get aliases
    const std::vector<std::string>& get_aliases() const;

    /// Get autocomplete strings
    std::vector<std::string> get_autocomplete_strings() const;

    /// Get short help text
    /// This text is displayed when help is requested without arguments
    std::string get_short_help_text() const;

    /// Get full help text
    /// This text is displayed when help is requested for this command specifically
    std::string get_full_help_text() const;

  private:
    std::vector<std::string> aliases_;
    std::vector<std::string> autocomplete_strings_;
};

App::App(std::string name, std::string description, App* parent)
    : CLI::App(std::move(name), std::move(description), parent) {
    set_help_flag("-h,--help", "Print this help message and exit");
}

App* App::add_subcommand(std::string name, std::string description) {
    subcommands_.emplace_back(new App(description, name, this));
    for (const auto& subc : subcommands_)
        if (subc.get() != subcommands_.back().get())
            if (subc->check_name(subcommands_.back()->get_name()) || subcommands_.back()->check_name(subc->get_name()))
                throw CLI::OptionAlreadyAdded(subc->get_name());

    return static_cast<App*>(subcommands_.back().get());
}

Option* App::add_option(std::string name, CLI::callback_t callback, std::string description, bool defaulted) {
    if (std::find_if(std::begin(options_), std::end(options_), [&name](const CLI::Option_p& option) {
            return option->get_name() == name;
        }) == std::end(options_)) {
        options_.emplace_back();
        CLI::Option_p& cliOption = options_.back();
        cliOption.reset(new Option(name, description, callback, defaulted, this));

        auto option = static_cast<Option*>(cliOption.get());
        option->copy_from(option_defaults_);
        return option;
    } else {
        throw CLI::OptionAlreadyAdded(name);
    }
}

App* App::autocomplete(std::vector<std::string> strings) {
    autocomplete_strings_ = std::move(strings);
    return this;
}

App* App::get_subcommand(const std::string& name) const { return static_cast<App*>(CLI::App::get_subcommand(name)); }

App* App::get_parent() { return static_cast<App*>(CLI::App::get_parent()); }

const App* App::get_parent() const { return static_cast<const App*>(CLI::App::get_parent()); }

const std::vector<std::string>& App::get_aliases() const { return aliases_; }

std::vector<std::string> App::get_autocomplete_strings() const { return autocomplete_strings_; }

std::string App::get_short_help_text() const {
    auto text = get_name() + " ";
    for (const auto& option : get_options()) {
        if (option->get_name() == "--help") {
            continue;
        }

        if (option->get_required()) {
            text += "<" + option->get_name() + ">" + " ";
        } else {
            text += "[" + option->get_name() + "]" + " ";
        }
    }

    text += "\t\t" + description_;

    return text;
}

std::string App::get_full_help_text() const {
    std::string text = "\nUsage: " + get_name() + " ";
    for (const auto& option : get_options()) {
        if (option->get_name() == "--help") {
            continue;
        }

        if (option->get_required()) {
            text += "<" + option->get_name() + ">" + " ";
        } else {
            text += "[" + option->get_name() + "]" + " ";
        }
    }

    if (!description_.empty()) {
        text += "\n\n" + description_;
    }

    text += "\n";

    return text;
}

} // namespace klap
