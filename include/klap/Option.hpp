#pragma once

#include "klap/Log.hpp"

#include "CLI/Option.hpp"

namespace klap {

class App;

class Option : public CLI::Option {
    friend App;

  protected:
    using CLI::Option::Option;

    template <class T> void copy_from(T& other) {
        group(other.get_group());
        required(other.get_required());
        ignore_case(other.get_ignore_case());
        configurable(other.get_configurable());
        multi_option_policy(other.get_multi_option_policy());
    }
};

} // namespace klap
