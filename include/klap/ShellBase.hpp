#pragma once

#include "klap/App.hpp"
#include "klap/Util.hpp"

#include <functional>
#include <string>
#include <vector>

namespace klap {

/// Base class for Interactive CLI app
class ShellBase : public App {
  public:
    ShellBase(std::string name, std::string description = "");

    /// Set prompt delimiter
    ShellBase* delimiter(std::string delimiter);

    /// Exit interactive CLI
    ShellBase* should_exit(bool should_exit);

    /// Turn hints on/off
    ShellBase* show_hints(bool show_hints);

    /// Set history filename
    ShellBase* history_filename(std::string history_filename);

    /// Run interactive CLI
    void run(int argc, char* argv[]);

    // Accessors
    const std::string& delimiter() const { return delimiter_; }
    bool should_exit() const { return should_exit_; }
    bool show_hints() const { return show_hints_; }

  protected:
    std::string delimiter_;
    bool should_exit_ = false;

    // Hints
    bool show_hints_ = true;
    size_t min_prefix_size_for_hint_ = 2;

    // History
    std::string history_filename_ = "./history.txt";
    int max_history_size_ = 100;

    // Current user input
    std::string input_;
};

ShellBase::ShellBase(std::string name, std::string description)
    : App(std::move(name), std::move(description), nullptr), delimiter_("\x1b[1;32m" + name + "\x1b[0m> ") {
    // Help command
    {
        auto help_command = add_subcommand("help");
        help_command->set_callback([this]() {
            std::cout << "\n  Commands:\n" << std::endl;
            for (const auto& subcommand : subcommands_) {
                auto subapp = static_cast<App*>(subcommand.get());
                std::cout << "    " << subapp->get_short_help_text() << std::endl;
            }

            std::cout << std::endl;
        });
    }

    // Exit command
    auto exit_command = add_subcommand("exit", "Exits application.");
    exit_command->set_callback([this]() { should_exit(true); });
}

ShellBase* ShellBase::delimiter(std::string delimiter) {
    delimiter_ = std::move(delimiter);
    return this;
}

ShellBase* ShellBase::should_exit(bool should_exit) {
    should_exit_ = should_exit;
    return this;
}

ShellBase* ShellBase::show_hints(bool show_hints) {
    show_hints_ = show_hints;
    return this;
}

ShellBase* ShellBase::history_filename(std::string history_filename) {
    history_filename_ = std::move(history_filename);
    return this;
}

void ShellBase::run(int argc, char* argv[]) {
    try {
        clear();
        parse(argc, argv);
    } catch (const CLI::CallForHelp& e) {
        std::cout << "pls help" << std::endl;
    } catch (const CLI::ParseError& e) {
        std::cerr << "error: " << e.what() << std::endl;
    }
}

} // namespace klap
