#pragma once

#include "klap/App.hpp"
#include "klap/ShellBase.hpp"
#include "klap/Util.hpp"

#include <replxx.hxx>

#include <functional>
#include <string>
#include <vector>

namespace klap {

/// Interactive CLI app useing replxx
class ReplxxShell : public ShellBase {
  public:
    ReplxxShell(std::string name, std::string description = "");

  private:
    replxx::Replxx rx_;

    // Hints
    replxx::Replxx::Color single_hint_color_ = replxx::Replxx::Color::GREEN;
};

ReplxxShell::ReplxxShell(std::string name, std::string description)
    : ShellBase(std::move(name), std::move(description)) {
    // Set up Replxx
    rx_.install_window_change_handler();
    rx_.history_load(history_filename_);
    rx_.set_max_history_size(max_history_size_);
    rx_.set_max_line_size(128);
    rx_.set_max_hint_rows(8);

    // Add Replxx autocomplete callback
    rx_.set_completion_callback(
        [this](const std::string& input, int break_pos, void* /*user_data*/) {
            replxx::Replxx::completions_t completions;

            std::string prefix = input.substr(break_pos);
            for (const auto& command : subcommands_) {
                if (command->get_name().compare(0, prefix.size(), prefix) == 0) {
                    completions.emplace_back(command->get_name().c_str());
                }
            }

            return completions;
        },
        nullptr);

    // Add Replxx hint callback
    rx_.set_hint_callback(
        [this](const std::string& input, int break_pos, replxx::Replxx::Color& color, void* /*user_data*/) {
            replxx::Replxx::hints_t hints;

            if (!show_hints_) {
                return hints;
            }

            std::string prefix = input.substr(break_pos);

            // Don't show hint if prefix is less than n characters
            if (prefix.size() < min_prefix_size_for_hint_) {
                return hints;
            }

            for (const auto& command : subcommands_) {
                if (command->get_name().compare(0, prefix.size(), prefix) == 0) {
                    hints.emplace_back(command->get_name().substr(prefix.size()).c_str());
                }
            }

            // Set different hint color if single match found
            if (hints.size() == 1) {
                color = single_hint_color_;
            }

            return hints;
        },
        nullptr);

    // Clear command
    auto clear_command = add_subcommand("clear");
    clear_command->set_callback([this]() { rx_.clear_screen(); });

    // History command
    auto history_command = add_subcommand("history");
    history_command->set_callback([this]() {
        for (auto i = 0; i < rx_.history_size(); ++i) {
            std::cout << rx_.history_line(i) << std::endl;
        }
    });

    // Interactive CLI
    set_callback([this]() {
        // Run in non-interactive mode if subcommand is run directly
        if (!get_subcommands().empty()) {
            return;
        }

        int ctrl_c_count = 0;

        while (!should_exit()) {
            // Get input from user
            char const* c_input = nullptr;
            do {
                c_input = rx_.input(delimiter_);
                // c_input = rx_.input("\x1b[1;32m" + name_ + "\x1b[0m> ");

                // Check if Ctrl+C was entered
                if (errno == EAGAIN) {
                    ++ctrl_c_count;
                    if (ctrl_c_count >= 2) {
                        should_exit(true);
                        break;
                    }

                    std::cout << "Input Ctrl+C again to exit" << std::endl;
                }
            } while ((c_input == nullptr) && (errno == EAGAIN));

            ctrl_c_count = 0;

            if (c_input == nullptr) {
                should_exit(true);
            }

            if (should_exit()) {
                break;
            }

            input_ = c_input;

            // std::cout << input_ << std::endl;

            // Check if user hit enter on an empty line
            if (input_.empty()) {
                continue;
            }

            // Build args from input
            auto args = detail::split(input_);
            std::reverse(std::begin(args), std::end(args));

            // Run subcommand
            try {
                // std::cout << "parsing " << input_ << std::endl;
                clear();
                parse(args);
                clear();
            } catch (const CLI::ParseError& e) {
                std::cerr << e.what() << std::endl;
            }

            rx_.history_add(input_);
            input_ = "";
        }

        rx_.history_save(history_filename_);
    });
}

} // namespace klap
