#pragma once

#include "klap/Util.hpp"

#ifdef WIN32
#include <windows.h>
#else
#include <termios.h>
#include <unistd.h>
#endif

namespace klap {

void set_stdin_echo(bool enable) {
#ifdef WIN32
    HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
    DWORD mode;
    GetConsoleMode(hStdin, &mode);

    if (!enable) {
        mode &= ~ENABLE_ECHO_INPUT;
    } else {
        mode |= ENABLE_ECHO_INPUT;
    }

    SetConsoleMode(hStdin, mode);

#else
    struct termios tty;
    tcgetattr(STDIN_FILENO, &tty);
    if (!enable) {
        tty.c_lflag &= ~ECHO;
    } else {
        tty.c_lflag |= ECHO;
    }

    (void)tcsetattr(STDIN_FILENO, TCSANOW, &tty);
#endif
}

/// Base class for Prompts
/// Uses CRTP so that fluent interface will work properly with subclass
template <class P> class PromptBase {
  public:
    PromptBase(std::string message);

    /// Prompts user for input and returns result
    template <class T = std::string> T read();

    /// Prompts user for input and returns result
    /// Turns off stdin echo so that user's input is not displayed in terminal
    std::string read_password();

    // Prompts user to choose from a list of options
    template <class T = std::string> T choose();

    /// Set default value
    P* default_value(std::string default_value);

    /// Set choices
    P* choices(std::vector<std::string> choices);

  private:
    std::string message_;
    std::string default_value_;
    std::vector<std::string> choices_;
};

/// Basic Prompt class
class Prompt : public PromptBase<Prompt> {
  public:
    using PromptBase<Prompt>::PromptBase;
};

template <class P> PromptBase<P>::PromptBase(std::string message) : message_(std::move(message)) {}

template <class P> template <class T> T PromptBase<P>::read() {
    std::cout << message_;
    std::string line;
    std::getline(std::cin, line);

    T t;
    CLI::detail::lexical_cast(line, t);
    return t;
}

template <class P> std::string PromptBase<P>::read_password() {
    std::cout << message_;
    std::string line;
    set_stdin_echo(false);
    std::getline(std::cin, line);
    set_stdin_echo(true);

    std::string s;
    CLI::detail::lexical_cast(line, s);
    return s;
}

template <class P> template <class T> T PromptBase<P>::choose() {
    std::cout << message_ << std::endl << std::endl;
    for (size_t i = 0; i < choices_.size(); ++i) {
        std::cout << i << ") " << choices_[i] << std::endl;
    }
    std::cout << "> ";

    std::string line;
    std::getline(std::cin, line);
    detail::trim(line);

    if (std::find(choices_.begin(), choices_.end(), line) != choices_.end()) {
        T t;
        CLI::detail::lexical_cast(line, t);
        return t;
    }

    try {
        auto i = std::stoul(line);
        if (i < choices_.size()) {
            T t;
            CLI::detail::lexical_cast(choices_[i], t);
            return t;
        }
    } catch (...) {
        return T{};
    }

    return T{};
}

template <class P> P* PromptBase<P>::default_value(std::string default_value) {
    default_value_ = std::move(default_value);
    return static_cast<P*>(this);
}

template <class P> P* PromptBase<P>::choices(std::vector<std::string> choices) {
    choices_ = std::move(choices);
    return static_cast<P*>(this);
}

} // namespace klap
