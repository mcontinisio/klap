#pragma once

#include "klap/App.hpp"
#include "klap/ShellBase.hpp"
#include "klap/Util.hpp"

#include <linenoise.hpp>

#include <functional>
#include <string>
#include <vector>

namespace klap {

/// Interactive CLI app using linenoise
class LinenoiseShell : public ShellBase {
  public:
    LinenoiseShell(std::string name, std::string description = "");
};

LinenoiseShell::LinenoiseShell(std::string name, std::string description)
    : ShellBase(std::move(name), std::move(description)) {
    // Set up linenoise
    linenoise::LoadHistory(history_filename_.c_str());
    linenoise::SetHistoryMaxLen(max_history_size_);

    // Add linenoise autocomplete callback
    linenoise::SetCompletionCallback([this](const char* editBuffer, std::vector<std::string>& completions) {
        std::string prefix{editBuffer};
        for (const auto& command : subcommands_) {
            if (command->get_name().compare(0, prefix.size(), prefix) == 0) {
                completions.emplace_back(command->get_name().c_str());
            }
        }
    });

    // Clear command
    auto clear_command = add_subcommand("clear");
    clear_command->set_callback([]() { linenoise::linenoiseClearScreen(); });

    // History command
    auto history_command = add_subcommand("history");
    history_command->set_callback([]() {
        const auto& history = linenoise::GetHistory();
        for (const auto& history_line : history) {
            std::cout << history_line << std::endl;
        }
    });

    // Interactive CLI
    set_callback([this]() {
        // Run in non-interactive mode if subcommand is run directly
        if (!get_subcommands().empty()) {
            return;
        }

        int ctrl_c_count = 0;

        while (!should_exit()) {
            // Get input from user
            bool should_quit = false;
            do {
                should_quit = linenoise::Readline(delimiter_.c_str(), input_);

                // Check if Ctrl+C was entered
                if (errno == EAGAIN) {
                    ++ctrl_c_count;
                    if (ctrl_c_count >= 2) {
                        should_exit(true);
                        break;
                    }

                    std::cout << "Input Ctrl+C again to exit" << std::endl;
                }
            } while (errno == EAGAIN);

            ctrl_c_count = 0;

            should_exit(should_quit);

            if (should_exit()) {
                break;
            }

            // std::cout << input_ << std::endl;

            // Check if user hit enter on an empty line
            if (input_.empty()) {
                continue;
            }

            // Build args from input
            auto args = detail::split(input_);
            std::reverse(std::begin(args), std::end(args));

            // Run subcommand
            try {
                // std::cout << "parsing " << input_ << std::endl;
                clear();
                parse(args);
                clear();
            } catch (const CLI::CallForHelp& e) {
                auto parsed_commands = get_subcommands();
                for (const auto* command : parsed_commands) {
                    auto subapp = static_cast<const App*>(command);
                    std::cout << subapp->get_full_help_text() << std::endl;
                }
            } catch (const CLI::ParseError& e) {
                std::cerr << "error: " << e.what() << std::endl;
            }

            linenoise::AddHistory(input_.c_str());
            input_ = "";
        }

        linenoise::SaveHistory(history_filename_.c_str());
    });
}

} // namespace klap
