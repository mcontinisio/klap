# klap

klap is a C++ library for building interactive CLI applications. It is built on top of [CLI11](https://github.com/CLIUtils/CLI11) and [replxx](https://github.com/AmokHuginnsson/replxx). It is inspired by libraries like [Vorpal](http://vorpal.js.org/) and [ishell](https://github.com/abiosoft/ishell).

## Getting started

./examples/simple.cpp

    #include "klap/Klap.hpp"

    int main(int argc, char* argv[]) {
        klap::Shell shell{"simple", "a simple shell"};
        shell.delimiter("> ")->show_hints(false);

        {
            shell.add_subcommand("hi")
                ->set_callback([]() {
                    std::cout << "hi" << std::endl;
                });
        }

        {
            std::string s;
            auto echoCommand = shell.add_subcommand("echo");
            echoCommand->add_option("string", s)->required();
            echoCommand->set_callback([&s]() {
                std::cout << s << std::endl;
                s = {};
            });
        }
        
        shell.run(argc, argv);

        return 0;
    }

Run app.

    $ ./examples/simple
    > hi
    hi
    > echo test
    test
    > exit

App can also be run non-interactively

    $ ./examples/simple echo test
    test
